const motCache = 'bonjour';
/**
 * renvoie un tableau qui contient le nombre de tiret correspondant au nombre de lettres a trouver dans le mot caché.
 * @param {number} nb le nombre de lettres que possède le mot caché
 * @return {Array} tableau qui contient le nombre de tiret correspondant au nombre de lettres a trouver dans le mot caché.
 */
const tiretInTab = (nb) => {
        let tab = [];
        for (i = 0; i < nb; i++) {
            tab[i] = '-';
        }
        return tab;
    }
    /**
     * Permet de vérifier si deux tableau sont identiques.
     * @param {Array} tab1 le premier tableau.
     * @param {Array} tab2 le deuxième tableau.
     * @return {boolean} true si les deux tableaux passé en paramètres sont identiques, false sinon.
     */
const verifTab = (tab1, tab2) => {
        let verif = true;
        let i = 0;
        while (verif === true && i < tab1.length) {
            if (tab1[i] !== tab2[i]) {
                verif = false;
            }
            i++;
        }
        return verif;
    }
    // le tableau qui se remplit au fur et a mesure que l'on trouve des lettres
let resultTab = tiretInTab(motCache.length);
const motCacheTab = motCache.split('');
// fonction principal du pendu
/**
 * Affiche a l'utilisateur si la lettre entré en console est dans le mot caché et sa place dans le mot
 * @param {string} valeur valeur de l'entrée en console.
 */
const lettresErreurs = [];

function pendu() {
    let lettres = document.getElementById("lettre").value;
    for (i = 0; i < motCache.length; i++) {
        if (motCache[i] === lettres) {
            document.getElementById('result').innerHTML = ' La lettre est bien présente dans le mot a l\'index :', i;
            resultTab[i] = lettres;
        }
    }
    lettresErreurs.push(lettres);
    console.log(lettresErreurs);
    document.getElementById('nbErreurs').innerHTML = lettresErreurs;

    document.getElementById('result').innerHTML = resultTab;
    if (verifTab(motCacheTab, resultTab)) {
        document.getElementById('result').innerHTML = 'YOU WIN ! c\'etait bien le mot ' + motCache;
    }
}