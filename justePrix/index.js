function nb_aleatoire(max) {
    return Math.floor(Math.random() * Math.floor(max));
}
// Le juste prix généré aléatoirement entre 0 et 20.
const nb = nb_aleatoire(20);
console.log(nb);
// Compte le nombre d'éssais pour trouver le juste prix.
let tour = -1;
// le chiffre que l'utilisateur renseigne pour tenter de trouver le juste prix.


function justePrix() {
    let chiffre = document.getElementById("chiffre").value;
    // la fonction trim() permet de retirer les éspaces en trop au debut et a la fin de la chaine de caractère sur laquelle elle est appelée.
    if (tour < 6) {
        // On indique a l'utilisateur si son chiffre est plus grand ou plus petit que le juste Prix.
        switch (true) {
            case chiffre < nb:
                document.getElementById('result').innerHTML = `<p>Votre chiffre  ${chiffre} est plus petit que le juste prix!</p>`;
                break;
            case chiffre > nb:
                document.getElementById('result').innerHTML = `<p>Votre chiffre  ${chiffre} est plus grand que le juste prix!</p>`;
                break;
            case chiffre == nb:
                document.getElementById('result').innerHTML = `BRAVO !! le juste prix etait bien : ${chiffre} vous avez trouvez en ${tour + 2} tour(s) !`;
                break;
            default:
                // Si toute autre entré qu'un chiffre est renseigné par l'utilisateur on lui indique que l'on a pas compris.
                // ATTENTION, cette action compte pour un tour quand même.
                document.getElementById('result').innerHTML = ' Je n\'ai pas compris`';
                break;
        }
        // Le programme s'arrête si on a dépasser le nombre de tour.
    } else {
        document.getElementById('result').innerHTML = ' Vous avez depasse votre nombre de tour le juste prix etait : ' + nb;
    }
    tour++;
    // On oublie pas le processus de demande de chiffre a l'utilisateur.
}