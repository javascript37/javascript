const { clavier } = require("./lib/clavier");

const NB = Math.floor(Math.random() * 20 + 1);
let nbTour = 0;

console.log("saisir un nombre entre 0 et 20");

function justePrix(valeur) {
    let chiffre = valeur.trim();
    while (nbTour < 6) {
        if (chiffre < NB) {
            console.log("trop petit");
        } else if (chiffre > NB) {
            console.log("trop grand");
        } else {
            console.log("bravo");
            return console.log("vous avez gagne en " + (++nbTour) + " coups");
            process.exit(0);
        }
        return ++nbTour;
    }
    console.log("perdu vous avez depasse le nombre d'essais");
}

clavier(justePrix);