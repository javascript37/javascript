const { clavier } = require("./lib/clavier");
// On déclare une constante qui récupére la fonction dans le fichier clavier.js
function nb_aleatoire(max) {
    return Math.floor(Math.random() * Math.floor(max));
}
// Le juste prix généré aléatoirement entre 0 et 20.
const nb = nb_aleatoire(20);
// Compte le nombre d'éssais pour trouver le juste prix.
let tour = -1;
// le chiffre que l'utilisateur renseigne pour tenter de trouver le juste prix.
let chiffre;
console.log('Entrez un chiffre entre 0 et 20...');
// A chaque ligne écrite.
function justePrix(valeur) {
    // la fonction trim() permet de retirer les éspaces en trop au debut et a la fin de la chaine de caractère sur laquelle elle est appelée.
    chiffre = valeur.trim();
    if (tour < 6) {
        // On indique a l'utilisateur si son chiffre est plus grand ou plus petit que le juste Prix.
        switch (true) {
            case chiffre < nb:
                console.log(`Votre chiffre  ${chiffre} est plus petit que le juste prix!`);
                break;
            case chiffre > nb:
                console.log(`Votre chiffre ${chiffre} est plus grand que le juste prix!`);
                break;
            case chiffre == nb:
                console.log(`BRAVO !! le juste prix était bien : ${chiffre} vous avez trouvez en ${tour + 2} tour(s) !`);
                process.exit(0);
                break;
            default:
                // Si toute autre entré qu'un chiffre est renseigné par l'utilisateur on lui indique que l'on a pas compris.
                // ATTENTION, cette action compte pour un tour quand même.
                console.log(' Je n\'ai pas compris  `' + line.trim() + '`');
                break;
        }
        // Le programme s'arrête si on a dépasser le nombre de tour.
    } else {
        console.log(' Vous avez dépassé votre nombre de tour le juste prix était : ' + nb)
        process.exit(0);
    }
    tour++;
    // On oublie pas le processus de demande de chiffre a l'utilisateur.
}
clavier(justePrix);