/**
 * Renvoie un jeux vide, c'est à dire une liste de 8 cases contenant '.'
 * @return un tableau de 8 '.'
 */
const nouveauJeu = () => {
        let tab = [];
        for (i = 0; i < 9; i++) {
            tab[i] = '.';
        }
        return tab;
    }
    /**
     * affiche un tableau passe en paramètre ( 3 valeur par ligne pour former une grille de morpion)
     * @param {Array} jeu un tableau contenant la valeurs du morpion et/ou des points
     * @return {void} affiche un tableau passe en paramètre
     */
const affiche = (jeu) => {
        document.getElementById('index0').innerHTML = jeu[0];
        document.getElementById('index1').innerHTML = jeu[1];
        document.getElementById('index2').innerHTML = jeu[2];
        document.getElementById('index3').innerHTML = jeu[3];
        document.getElementById('index4').innerHTML = jeu[4];
        document.getElementById('index5').innerHTML = jeu[5];
        document.getElementById('index6').innerHTML = jeu[6];
        document.getElementById('index7').innerHTML = jeu[7];
        document.getElementById('index8').innerHTML = jeu[8];
    }
    /**
     * permet de savoir qui dois jouer à partir d'un jeu passe en paramètre.
     * @param {Array} jeu  un tableau contenant les valeurs du morpion et/ou des points.
     * @return {string} 'X' si c'est a son tour de jouer ou 'O' sinon.
     */
    //Creation d'une fonction joueur qui permet de savoir quel joueur doit jouer
const joueur = (jeu) => {
        let nbX = 0;
        let nbO = 0;
        let joueur = "";
        let bool = true;
        for (i = 0; i < jeu.length; i++) {
            if (jeu[i] == "X") {
                nbX++;
            }
            if (jeu[i] == "O") {
                nbO++;
            }
            if (jeu[i] == ".") {
                bool = false;
            }
        }
        if (bool == true) {
            document.getElementById('tour').innerHTML = "Le jeu est termine";
            gagner(jeu)
        } else {
            if (nbX <= nbO) {
                document.getElementById('tour').innerHTML = "joueur X c'est votre tour";
                joueur = "X";
            } else if (nbX > nbO) {
                document.getElementById('tour').innerHTML = "joueur O c'est votre tour";
                joueur = "O";
            }
        }
        return joueur;
    }
    /**
     * Renvoie les coups possible à partir d'un jeu passe en paramètre.
     * @param {Array} jeu  un tableau contenant les valeurs du morpion et/ou des points.
     * @return {Array} un tableau avec les valeurs possible pour les prochain tours de jeux.
     */
const coupPossibles = (jeu) => {
        let coups = [];
        for (i = 0; i < jeu.length; i++) {
            if (jeu[i] === '.') {
                coups.push(i);
            }
        }
        return coups;
    }
    /**
     * permet de savoir qui a gagne ou perdu.
     * @param {Array} jeu  un tableau contenant les valeurs du morpion et/ou des points.
     * @return 1 si le joueur X a gagne, -1 si le joueur O a gagne, 0 si personne n'a gagne, même si il reste des coups possible.
     */
const gagner = (jeu) => {
        let res = '.'
        let resultat = '';
        if (jeu[0] == jeu[3] && jeu[0] == jeu[6]) {
            res = jeu[0]
        }
        if (jeu[1] == jeu[4] && jeu[1] == jeu[7]) {
            res = jeu[1]
        }
        if (jeu[2] == jeu[5] && jeu[2] == jeu[8]) {
            res = jeu[2]
        }
        if (jeu[0] == jeu[1] && jeu[0] == jeu[2]) {
            res = jeu[0]
        }
        if (jeu[4] == jeu[5] && jeu[4] == jeu[3]) {
            res = jeu[3]
        }
        if (jeu[7] == jeu[8] && jeu[7] == jeu[6]) {
            res = jeu[6]
        }
        if (jeu[0] == jeu[4] && jeu[0] == jeu[8]) {
            res = jeu[0]
        }
        if (jeu[2] == jeu[4] && jeu[2] == jeu[6]) {
            res = jeu[2]
        }
        if (res == 'O') {
            resultat = -1;
            document.getElementById('result').innerHTML = 'c\'est le joueur qui avait les ronds qui a gagne';
        } else {
            if (res == 'X') {
                document.getElementById('result').innerHTML = 'c\'est le joueur qui avait les croix qui a gagne';
                resultat = 1;
            } else {
                document.getElementById('result').innerHTML = 'egalite ! ';
                resultat = 0;
            }
        }
        return resultat
    }
    /**
     * Permet de jouer un coup dans une case precise.
     * @param {Array} jeu  un tableau contenant les valeurs du morpion et/ou des points.
     * @param {number} coup chiffre corespondant a la case ou l'on veut jouer.
     * @return {void} afiche le jeu
     */
let jeu = nouveauJeu(); // on initialise un nouveau jeux
const jouer = () => {
    // on demande une valeur a l'utilisateur
    let coup = document.getElementById("indexjoue").value;
    //au debut et à la fin de la chaine de caractère sur laquelle elle est appelee
    let joueurXO = joueur(jeu); // si c'est une croix ou un rond
    let tab = coupPossibles(jeu); // un tableau qui contient les coups possibles
    for (i = 0; i < tab.length; i++) {
        if (tab[i] == coup) {
            jeu[coup] = joueurXO; //= > jeu[tab[i]] = joueurXO
        }
    }
    affiche(jeu);
}

function myFunction() {
    document.getElementById("myDIV").style.display = "block";
}